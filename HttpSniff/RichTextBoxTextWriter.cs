﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace HttpSniff {
	public class RichTextBoxTextWriter : TextWriter {
		public override Encoding Encoding => Encoding.UTF8;
		private RichTextBox _TextBox;

		public RichTextBoxTextWriter(RichTextBox tb) {
			this._TextBox = tb;
		}

		private delegate void WriteLineDelegate(string str);
		public override void WriteLine(string str) {
			this._TextBox.Dispatcher.Invoke(new WriteLineDelegate(WriteLineInvoked), str);
		}
		private void WriteLineInvoked(string str) {
			this._TextBox.AppendText(str + "\r");
			this._TextBox.ScrollToEnd();
		}
	}
}
