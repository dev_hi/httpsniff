﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HttpSniff.Extensions {
	public class ID3 {
		private Dictionary<string, byte[]> tags = new Dictionary<string, byte[]>();
		private static Encoding ASCII = Encoding.GetEncoding("ISO-8859-1");
		private static Encoding UCS2 = new UnicodeEncoding();
		private static Encoding UTF16BEwoBOM = new UnicodeEncoding(true, false, false);
		private static Encoding UTF8 = Encoding.UTF8;

		static string GetString(byte[] buf) {
			Encoding enc = Encoding.GetEncoding(51949);
			int a = 1;
			switch(buf[0]) {
				case 0:
					//enc = ASCII;
					//use EUCKR for KTMusic data
					break;
				case 1:
					enc = UCS2;
					a = 3;
					break;
				case 2:
					enc = UTF16BEwoBOM;
					break;
				case 3:
					enc = UTF8;
					break;
			}

			return enc.GetString(buf, a, buf.Length - a);
		}

		public string Title => GetString(tags["TIT2"]);
		public string Artist => GetString(tags["TPE1"]);
		public string Album => GetString(tags["TALB"]);
		public int Disk => int.Parse(GetString(tags["TPOS"]));
		public int Track => int.Parse(GetString(tags["TRCK"]));
		public int Year => int.Parse(GetString(tags["TYER"]));
		public string Genre => GetString(tags["TCON"]);

		public ID3(byte[] buf) {
			if(buf[0] == 0x49 && buf[1] == 0x44 && buf[2] == 0x33) {
				uint tlen = BitConverter.ToUInt32(buf, 6);
				for(uint i = 10; i < tlen;) {
					if(buf[i] > 47 && buf[i] < 91) {
						string hdrstr = Encoding.ASCII.GetString(buf, (int)i, 4);
						i += 4;
						byte[] lenb = new byte[4];
						Array.Copy(buf, i, lenb, 0, 4);
						lenb = lenb.Reverse().ToArray();
						uint len = BitConverter.ToUInt32(lenb, 0);
						if(len > tlen) break;
						i += 4;
						i += 2;
						if(!tags.ContainsKey(hdrstr)) {
							tags.Add(hdrstr, new byte[len]);
							for(uint j = 0; j < len; ++j) {
								tags[hdrstr][j] = buf[i + j];
							}
						}
						i += len;
					}
					else break;
				}
			}
		}
	}
}
