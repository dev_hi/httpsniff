﻿using HttpSniff.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace HttpSniff.Extensions {
	public static class Utils {


		public static string ReadLine(this MemoryStream ms, int bufsize = 0, Encoding enc = null) {
			enc = enc == null ? Encoding.ASCII : enc;
			MemoryStream ms2 = new MemoryStream(bufsize);
			byte p = 0;
			byte c = (byte)ms.ReadByte();
			while(p != 0x0D && c != 0x0A && ms.CanRead) {
				if(c != 0x0D && c != 0x0A) ms2.WriteByte(c);
				p = c;
				c = (byte)ms.ReadByte();
				if(bufsize != 0 && ms2.Position + 1 >= bufsize) {
					ms2.Close();
					return "";
				}
			}
			byte[] ret = ms2.ToArray();
			ms2.Close();
			return enc.GetString(ret);
		}

		public static string Hash(this byte[] buf) {
			using(SHA1Managed sha1 = new SHA1Managed()) {
				var hash = sha1.ComputeHash(buf);
				var sb = new StringBuilder(hash.Length * 2);

				foreach(byte b in hash) {
					sb.Append(b.ToString("X2"));
				}

				return sb.ToString();
			}
		}

		public static bool GetBit(this byte b, int i) {
			return ((b >> i) & 0x01) == 1;
		}
		public static void SetBit(this byte b, int i, bool value) {
			if(value) b |= (byte)(1 << i);
			else b &= (byte)(~(1 << i));
		}
		public static byte MakeBit(int len) {
			byte ret = 0;
			for(int j = 0; j < len; ++j) ret |= (byte)(1 << j);
			return ret;
		}
		public static byte GetBits(this byte b, int i, int len) {
			return (byte)((b & (MakeBit(len) << i)) >> i);
		}
		public static void SetBits(this byte b, int i, int len, byte value) {
			byte s = MakeBit(len);
			value &= s;
			value <<= i;
			s <<= i;
			b = (byte)((b & (~s)) | value);
		}
		public static int BitCount(this uint a) {
			int ret = 0;
			for(int i = 0; i < 32; ++i) {
				if((a & 1) == 1) ++ret;
			}
			return ret;
		}

		public static void StopService(string scname) {
			try {
				ServiceController sc = new ServiceController(scname);
				if(sc.Status == ServiceControllerStatus.Running) {
					Logger.Log("service {0} shutdown...", sc.ServiceName);
					sc.Stop();
				}
			}
			catch(InvalidOperationException) { }
		}
	}
}
