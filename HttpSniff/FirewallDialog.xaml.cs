﻿using HttpSniff.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HttpSniff {
	/// <summary>
	/// FirewallDialog.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class FirewallDialog : Window {
		private Regex _CIDRCheck = new Regex("([0-9]{1,3}\\.?){4}\\/[0-9]{1,2}");
		private CIDRMask _FirewallMask;

		public FirewallDialog(CIDRMask mask) {
			this._FirewallMask = mask;
			InitializeComponent();
			this.cidrlist.ItemsSource = this._FirewallMask;
		}

		private void AddClick(object sender, RoutedEventArgs e) {
			if(this._CIDRCheck.IsMatch(this.cidrinput.Text)) {
				this._FirewallMask.AddMask(this.cidrinput.Text);
			}
		}

		private void RemoveClick(object sender, RoutedEventArgs e) {
			this._FirewallMask.RemoveMask(this.cidrlist.SelectedIndex);
		}

		private void CloseClick(object sender, RoutedEventArgs e) {
			this.Close();
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			e.Cancel = true;
			this.Visibility = Visibility.Hidden;
		}
	}
}
