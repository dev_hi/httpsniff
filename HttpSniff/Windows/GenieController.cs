﻿using HttpSniff.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HttpSniff.Windows {
	public class GenieController {
		public const int IDC_PLAYLIST = 0x00002739;

		public const string CLS_MWND = "#32770";
		public const string CLS_PLAYLIST = "AfxWnd120u";
		public const string CLS_IESERVER_CONT = "AfxFrameOrView120u";
		public const string CAP_MWND = "지니뮤직";
		public const string CAP_PLAYLIST = "";
		public const string CAP_IESERVER_CONT = "";

		public const int RPR_PLAY_X1 = 128;
		public const int RPR_PLAY_Y1 = 236;
		public const int RPR_PLAY_X2 = 170;
		public const int RPR_PLAY_Y2 = 279;

		public const int RPR_NEXT_X1 = 174;
		public const int RPR_NEXT_Y1 = 236;
		public const int RPR_NEXT_X2 = 216;
		public const int RPR_NEXT_Y2 = 279;

		public const int RPR_MULIST1_X1 = 0;
		public const int RPR_MULIST1_Y1 = 0;
		public const int RPR_MULIST1_X2 = 280;
		public const int RPR_MULIST1_Y2 = 25;

		private Process GenieProcess = null;
		private int hWndGenieMusicMain = 0;
		private int hWndGenieMusicPlaylist = 0;
		private int hWndGenieIEServerContainer = 0;

		private bool Initialized = false;
		private bool ProcessRunning = false;

		public GenieController() { }

		public void Init() {
			try {
				if(!this.Initialized) {
					Logger.Log("wait for geniemusic.exe process.");
					while(this.GenieProcess == null) {
						Process[] p = Process.GetProcessesByName("geniemusic");
						if(p.Length > 0) {
							this.GenieProcess = p[0];
							Logger.Log("found process. (PID={0})", this.GenieProcess.Id);
							this.ProcessRunning = true;
						}
						else {
							Thread.Sleep(50);
						}
					}
					this.GenieProcess.EnableRaisingEvents = true;
					this.GenieProcess.Exited += this.GenieProcess_Exited;

					Logger.Log("wait for hWndGenieMusicMain");
					this.hWndGenieMusicMain = 0;
					while(this.hWndGenieMusicMain == 0) {
						this.hWndGenieMusicMain = User32.FindWindowOfProcessId(this.GenieProcess.Id, CLS_MWND, CAP_MWND);
						if(this.hWndGenieMusicMain == 0) Thread.Sleep(50);
					}
					Logger.Log(String.Format("found hWndGenieMusicMain (0x{0:X})", this.hWndGenieMusicMain));
					this.hWndGenieIEServerContainer = User32.FindWindowEx(this.hWndGenieMusicMain, 0, CLS_IESERVER_CONT, CAP_IESERVER_CONT);
					Logger.Log(String.Format("found hWndGenieIEServerContainer (0x{0:X})", this.hWndGenieIEServerContainer));
					this.hWndGenieMusicPlaylist = this.GetWindow(4, CLS_PLAYLIST, CAP_PLAYLIST);
					Logger.Log(String.Format("found hWndGenieMusicPlaylist (0x{0:X})", this.hWndGenieMusicPlaylist));
					this.Initialized = true;
				}
			}
			catch(Exception e) {
				Logger.Error("GenieController got an error : {0}", e.Message);
				Logger.Error("GenieController re-initializing.");
				this.Init();
			}
		}

		private void GenieProcess_Exited(object sender, EventArgs e) {
			Logger.Warn("geniemusic.exe process (PID={0}) has been terminated. GenieController re-initializing.", this.GenieProcess.Id);
			this.GenieProcess = null;
			this.hWndGenieMusicMain = 0;
			this.hWndGenieMusicPlaylist = 0;
			this.hWndGenieIEServerContainer = 0;
			this.Initialized = false;
			this.ProcessRunning = false;
			this.Init();
		}

		/*private bool CheckProcess() {
			if(this.GenieProcess.HasExited) {

			}
		}*/
		
		public int GetWindow(int index, string classname, string windowname) {
			if(this.ProcessRunning) {
				int childafter = 0;
				for(int i = 0; i <= index; ++i) {
					childafter = User32.FindWindowEx(this.hWndGenieMusicMain, childafter, classname, windowname);
				}
				return childafter;
			}
			return 0;
		}

		public int GetStandardXPosition() {
			if(!this.Initialized || !this.ProcessRunning) return 0;
			User32.RECT mainrect = new User32.RECT();
			User32.RECT ierect = new User32.RECT();

			User32.GetWindowRect(this.hWndGenieMusicMain, ref mainrect);
			User32.GetWindowRect(this.hWndGenieIEServerContainer, ref ierect);
			int abrelx;
			if(User32.IsWindowVisible(this.hWndGenieIEServerContainer)) abrelx = ierect.right - mainrect.left;
			else abrelx = mainrect.left;

			return abrelx;
		}

		public User32.RECT PlayButtonRect {
			get {
				int x = GetStandardXPosition();
				return new User32.RECT() {
					left = x + RPR_PLAY_X1,
					top = RPR_PLAY_Y1,
					right = x + RPR_PLAY_X2,
					bottom = RPR_PLAY_Y2
				};
			}
		}
		public User32.RECT NextButtonRect {
			get {
				int x = GetStandardXPosition();
				return new User32.RECT() {
					left = x + RPR_NEXT_X1,
					top = RPR_NEXT_Y1,
					right = x + RPR_NEXT_X2,
					bottom = RPR_NEXT_Y2
				};
			}
		}
		public User32.RECT MusiclistCurrentRect {
			get {
				//int x = GetStandardXPosition();
				return new User32.RECT() {
					left = RPR_MULIST1_X1,
					top = RPR_MULIST1_Y1,
					right = RPR_MULIST1_X2,
					bottom = RPR_MULIST1_Y2
				};
			}
		}

		public void ClickRandomPosOnRect(User32.RECT a) {
			if(!this.Initialized || !this.ProcessRunning) return;
			Random r = new Random();
			int x = r.Next(a.left, a.right);
			int y = r.Next(a.top, a.bottom);
			this.Click(x, y);
		}

		public void DoubleClickRandomPosOnRect(User32.RECT a) {
			if(!this.Initialized || !this.ProcessRunning) return;
			Random r = new Random();
			int x = r.Next(a.left, a.right);
			int y = r.Next(a.top, a.bottom);
			this.DoubleClick(x, y);
		}

		public void DoubleClick(int x, int y) {
			if(!this.Initialized || !this.ProcessRunning) return;
			Logger.Log("make double click event ({0}, {1})", x, y);

			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_PARENTNOTIFY, User32.WM_LBUTTONDOWN, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEMOVE, 0, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_LBUTTONDOWN, User32.MK_LBUTTON, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_LBUTTONUP, 0, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_LBUTTONDBLCLK, User32.MK_LBUTTON, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_LBUTTONUP, 0, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEMOVE, 0, (y << 16) | x);

			/*
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_PARENTNOTIFY, User32.WM_LBUTTONDOWN, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicMain, 0x02000001);
			User32.PostMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEMOVE, 0, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			Thread.Sleep(20);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_PARENTNOTIFY, User32.WM_LBUTTONDOWN, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_PARENTNOTIFY, User32.WM_LBUTTONDOWN, (y << 16) | x);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_MOUSEACTIVATE, this.hWndGenieMusicMain, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02010001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02000001);
			User32.SendMessage(this.hWndGenieMusicMain, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			User32.SendMessage(this.hWndGenieMusicPlaylist, User32.WM_SETCURSOR, this.hWndGenieMusicPlaylist, 0x02020001);
			*/
			/*
			this.Click(this.hWndGenieMusicPlaylist, x, y);
			Thread.Sleep(100);
			this.Click(this.hWndGenieMusicPlaylist, x, y);*/
		}

		public void Click(int x, int y) {
			this.Click(this.hWndGenieMusicMain, x, y);
		}

		public void Click(int hWnd, int x, int y) {
			if(!this.Initialized || !this.ProcessRunning) {
				Logger.Warn("make mouse click event ({0}, {1})", x, y);
				return;
			}

			Logger.Log("make mouse click event ({0}, {1})", x, y);

			User32.SendMessage(hWnd, User32.WM_SETCURSOR, hWnd, 0x02000001);
			User32.PostMessage(hWnd, User32.WM_MOUSEMOVE, 0, (y << 16) | x);
			Thread.Sleep(10);
			User32.SendMessage(hWnd, User32.WM_SETCURSOR, hWnd, 0x02010001);
			User32.PostMessage(hWnd, User32.WM_LBUTTONDOWN, User32.MK_LBUTTON, (y << 16) | x);
			Thread.Sleep(10);
			User32.SendMessage(hWnd, User32.WM_SETCURSOR, hWnd, 0x02000001);
			User32.PostMessage(hWnd, User32.WM_MOUSEMOVE, 0, (y << 16) | x);
			Thread.Sleep(10);
			User32.SendMessage(hWnd, User32.WM_SETCURSOR, hWnd, 0x02020001);
			User32.PostMessage(hWnd, User32.WM_LBUTTONUP, 0, (y << 16) | x);
			Thread.Sleep(10);
		}
	}
}
