﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HttpSniff.Network;
using HttpSniff.Logging;

namespace HttpSniff.Sniffer {
	public delegate void TcpStreamEventHandler(TcpStream sender, TcpStreamEventArgs e);
	public class TcpStream : IDisposable {
		public enum TcpHandshakeStatus {
			NoConnection, SynchronizationRequested, SynchronizationAccepted, Established, Finished
		}

		public int OutboundStreamBufferLength = 0;
		public uint LocalReferenceSequenceNumber = 0;
		public uint LocalRelativeSequenceNumber = 0;
		public bool LocalFinishRequested = false;
		public bool LocalFinishAccepted = false;
		public uint LocalCurrentSequenceNumber { get { return this.LocalReferenceSequenceNumber + this.LocalRelativeSequenceNumber; } }

		public int InboundStreamBufferLength = 0;
		public uint RemoteReferenceSequenceNumber = 0;
		public uint RemoteRelativeSequenceNumber = 0;
		public bool RemoteFinishRequested = false;
		public bool RemoteFinishAccepted = false;
		public uint RemoteCurrentSequenceNumber { get { return this.RemoteReferenceSequenceNumber + this.RemoteRelativeSequenceNumber; } }

		public TcpHandshakeStatus HandshakeStatus = TcpHandshakeStatus.NoConnection;
		public IPEndPoint LocalhostEndpoint;
		public IPEndPoint RemotehostEndpoint;

		public MemoryStream InboundBuffer = new MemoryStream();
		public MemoryStream OutboundBuffer = new MemoryStream();

		public TcpStreamEventHandler OnEstablished;
		public TcpStreamEventHandler OnPayloadArrival;
		public TcpStreamEventHandler OnPacketPush;
		public TcpStreamEventHandler OnFinished;

		public bool FromRemote = false;

		public TcpStream(IPAddress localip, int localport, IPAddress remoteip, int remoteport, bool fromremote) {
			this.FromRemote = fromremote;
			this.LocalhostEndpoint = new IPEndPoint(localip, localport);
			this.RemotehostEndpoint = new IPEndPoint(remoteip, remoteport);
			Logger.Log("Stream Captured. {0}", this.ToString());
		}

		public bool CheckEndpoint(IPAddress srcip, int srcport, IPAddress destip, int destport) {
			return this.LocalhostEndpoint.Address.Equals(srcip) && this.LocalhostEndpoint.Port.Equals(srcport) && this.RemotehostEndpoint.Address.Equals(destip) && this.RemotehostEndpoint.Port.Equals(destport) ||
					this.RemotehostEndpoint.Address.Equals(srcip) && this.RemotehostEndpoint.Port.Equals(srcport) && this.LocalhostEndpoint.Address.Equals(destip) && this.LocalhostEndpoint.Port.Equals(destport);
		}

		public void TcpPacketHandle(bool fromremote, TcpPacket p) {
			Logger.Log("{0}", p.ToString());
			switch(this.HandshakeStatus) {
				case TcpHandshakeStatus.NoConnection:
					if(p.Flags == 0x0002) {
						if(fromremote) {
							this.RemoteReferenceSequenceNumber = p.SequenceNumber;
							this.RemoteRelativeSequenceNumber++;
						}
						else {
							this.LocalReferenceSequenceNumber = p.SequenceNumber;
							this.LocalRelativeSequenceNumber++;
						}
						this.HandshakeStatus = TcpHandshakeStatus.SynchronizationRequested;
						Logger.Log("synchronization requested.");
					}
					else {
						this.Destroy(String.Format("invalid flag {0:X4}, must be 0x0002 at NoConnection", p.Flags));
						return;
					}
					break;
				case TcpHandshakeStatus.SynchronizationRequested:
					if(p.Flags == 0x0012) {
						if(fromremote) {
							this.RemoteReferenceSequenceNumber = p.SequenceNumber;
							this.RemoteReferenceSequenceNumber++;
						}
						else {
							this.LocalReferenceSequenceNumber = p.SequenceNumber;
							this.LocalRelativeSequenceNumber++;
						}
						this.HandshakeStatus = TcpHandshakeStatus.SynchronizationAccepted;
						Logger.Log("synchronization accepted.");
					}
					else {
						this.Destroy(String.Format("invalid flag {0:X4}, must be 0x0012 at SynchronizationRequested", p.Flags));
						return;
					}
					break;
				case TcpHandshakeStatus.SynchronizationAccepted:
					if(p.Flags == 0x0010) {
						this.HandshakeStatus = TcpHandshakeStatus.Established;
						Logger.Log("tcp established ({0})", this.ToString());
						Logger.Log("connection established.");
						TcpStreamEventArgs e = new TcpStreamEventArgs(this, TcpStreamEventArgs.TcpStreamEvent.Established, fromremote);
						this.OnEstablished?.Invoke(this, e);
					}
					else {
						this.Destroy(String.Format("invalid flag {0:X4}, must be 0x0010 at SynchronizationAccepted", p.Flags));
						return;
					}
					break;
				case TcpHandshakeStatus.Established:
					uint seqidx = p.SequenceNumber - (fromremote ?  this.RemoteReferenceSequenceNumber : this.LocalReferenceSequenceNumber);

					if(p.ACK) {
						if(p.PayloadData.Length > 0) {
							MemoryStream target = fromremote ? this.InboundBuffer : this.OutboundBuffer;
							uint idxalign;
							if(fromremote) {
								this.InboundStreamBufferLength += p.PayloadData.Length;
								idxalign = this.RemoteRelativeSequenceNumber;
								target = this.InboundBuffer;
							}
							else {
								this.OutboundStreamBufferLength += p.PayloadData.Length;
								idxalign = this.LocalRelativeSequenceNumber;
								target = this.OutboundBuffer;
							}
							try {
								target.Seek(seqidx, SeekOrigin.Begin);
								target.Write(p.PayloadData, 0, p.PayloadData.Length);
							}
							catch(Exception ex) {
								Logger.Error("Buffer got an error : {0}", ex.Message);
								return;
							}

							TcpStreamEventArgs e = new TcpStreamEventArgs(this, TcpStreamEventArgs.TcpStreamEvent.PacketArrival, fromremote);
							e.Buffer = p.PayloadData;
							this.OnPayloadArrival?.Invoke(this, e);
						}
						else {
							if(this.RemoteFinishRequested && !fromremote) this.RemoteFinishAccepted = true;
							else if(this.LocalFinishRequested && fromremote) this.LocalFinishAccepted = true;
						}
					}
					if(p.PSH) {
						if(fromremote) {
							this.InboundStreamBufferLength = 0;
						}
						else {
							this.OutboundStreamBufferLength = 0;
						}
						TcpStreamEventArgs e = new TcpStreamEventArgs(this, TcpStreamEventArgs.TcpStreamEvent.PacketPush, fromremote);
						this.OnPacketPush?.Invoke(this, e);
					}
					break;
			}
			if(fromremote) {
				this.RemoteRelativeSequenceNumber += (uint)p.PayloadData.Length;
			}
			else {
				this.LocalRelativeSequenceNumber += (uint)p.PayloadData.Length;
			}
			if(p.FIN) {
				if(fromremote) {
					this.RemoteFinishRequested = true;
					this.RemoteRelativeSequenceNumber++;
				}
				else {
					this.LocalFinishRequested = true;
					this.LocalRelativeSequenceNumber++;
				}
			}
			if(this.RemoteFinishAccepted && this.LocalFinishAccepted) {
				TcpStreamEventArgs e = new TcpStreamEventArgs(this, TcpStreamEventArgs.TcpStreamEvent.Finished, fromremote);
				this.OnFinished?.Invoke(this, e);
			}
		}

		public void FlushLocalBuffer() {

		}

		public void Destroy(string reason) {
			Logger.Warn("stream destroyed. reason : {0}", reason);
			this.Dispose(true);
		}

		public override string ToString() {
			return this.ToString("{0} <=> {1}");
		}

		public string ToString(string format) {
			return String.Format(format, this.LocalhostEndpoint.ToString(), this.RemotehostEndpoint.ToString(), this.LocalReferenceSequenceNumber, this.LocalRelativeSequenceNumber, this.LocalCurrentSequenceNumber, this.RemoteReferenceSequenceNumber, this.RemoteRelativeSequenceNumber, this.RemoteCurrentSequenceNumber);
		}

		#region IDisposable Support
		private bool disposedValue = false; 
		protected virtual void Dispose(bool disposing) {
			Logger.Log("stream({0}) disposing.", this.ToString());
			if(!disposedValue) {
				if(disposing) {
					this.OnEstablished = null;
					this.OnFinished = null;
					this.OnPacketPush = null;
					this.OnPayloadArrival = null;
					this.InboundBuffer.Close();
					this.OutboundBuffer.Close();
				}

				disposedValue = true;
			}
		}

		~TcpStream()
		{
		  Dispose(false);
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
