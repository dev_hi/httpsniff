﻿using HttpSniff.Extensions;
using HttpSniff.Logging;
using HttpSniff.Network;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using WinDivert;
using WinDivert.Invoke;

namespace HttpSniff.Sniffer {
	public delegate void TcpCapturedHandler(TcpPacket packet);
	public class TcpSniffer {
		public bool Running => WinDivertInvoke.IsRunning();
		public int MTU = 1500;
		public string Netmask { set { this._RemoteNetmask.AddMask(value); } }
		public string DivertFilter = "tcp";
		public bool SnifferRunning = false;
		public bool Initialized = false;

		private IntPtr _WinDivertHandle;
		private CIDRMask _LocalNetmask = new CIDRMask();
		private CIDRMask _RemoteNetmask = new CIDRMask();
		public CIDRMask _DropNetmask = new CIDRMask();
		public ConcurrentQueue<IPPacket> CaptureProcessingQueue = new ConcurrentQueue<IPPacket>();

		public TcpCapturedHandler OnCaptured;

		public TcpSniffer() {
			this._DropNetmask.AddMask("254.255.255.254/32");
			string host = Dns.GetHostName();
			IPHostEntry entry = Dns.GetHostEntry(host);
			foreach(IPAddress addr in entry.AddressList) {
				this._LocalNetmask.AddMask(addr.ToString());
			}
			WinDivertInvoke.SetCallback(OnPacketArrival);
			WinDivertInvoke.SetErrorCallback(OnErrorRaised);
		}

		public void Start() {
			this.SnifferRunning = true;
			this._WinDivertHandle = WinDivertInvoke.StartSniff(DivertFilter, this.MTU, 0);
			if(this._WinDivertHandle.ToInt32() == -1) {
				Logger.Error("an error has been ocurred while init WinDivert.");
				return;
			}
			this.RunQueueProcessThread();
			this.Initialized = true;
			Logger.Log("WinDivert driver installed, and capture thread has been started.");
		}

		public void Stop() {
			this.SnifferRunning = false;
			Logger.Log("shutting down...");
			WinDivertInvoke.StopSniff(this._WinDivertHandle);
			Logger.Log("WinDivert driver closed.");
			Utils.StopService("WinDivert");
			Logger.Log("WinDivert service stopped and removed.");
		}

		private bool OnPacketArrival(IntPtr pPacket, uint packet_len, WINDIVERT_ADDRESS[] pAddr, uint addr_len) {
			IPPacket ip = new IPPacket(pPacket, packet_len);

			if(SnifferRunning) {
				bool fromlocal = this._LocalNetmask.CheckMask(ip.SourceAddress);
				bool tolocal = this._LocalNetmask.CheckMask(ip.DestinationAddress);
				bool fromremote = this._RemoteNetmask.CheckMask(ip.SourceAddress);
				bool toremote = this._RemoteNetmask.CheckMask(ip.DestinationAddress);
				if(_DropNetmask.CheckMask(ip.SourceAddress) || _DropNetmask.CheckMask(ip.DestinationAddress)) {
					Logger.Log("dropped packet {0} to {1}", ip.SourceAddress, ip.DestinationAddress);
					return false;
				}
				if(fromlocal && toremote || fromremote && tolocal) {
					this.CaptureProcessingQueue.Enqueue(ip);
					this.RunQueueProcessThread();
				}
			}

			return true;
		}

		private bool QueueProcessThreadRunning = false;
		private Thread QueueProcessThread;

		private void RunQueueProcessThread() {
			if(!this.QueueProcessThreadRunning) {
				this.QueueProcessThreadRunning = true;
				//if(this.QueueProcessThread != null) if(this.QueueProcessThread.IsAlive) this.QueueProcessThread.Interrupt();
				this.QueueProcessThread = new Thread(new ThreadStart(QueueProcessThreadWork));
				this.QueueProcessThread.Start();
			}
		}

		public WinDivertInvoke.ErrorCallback OnError;

		private void OnErrorRaised(ulong error) {
			Logger.Error("native error (0x{0:X8}) raised.", error);
			this.OnError?.Invoke(error);
		}


		private void QueueProcessThreadWork() {
			this.QueueProcessThreadRunning = true;
			try {
				IPPacket ip;
				while(this.CaptureProcessingQueue.TryDequeue(out ip)) {
					this.HandlePacket(ip);
				}
			}
			catch(ThreadInterruptedException) {
				this.QueueProcessThreadRunning = false;
			}
			this.QueueProcessThreadRunning = false;
		}

		private void HandlePacket(IPPacket ip) {
			TcpPacket tcp = new TcpPacket(ip);
			this.OnCaptured?.Invoke(tcp);
		}
	}
}
