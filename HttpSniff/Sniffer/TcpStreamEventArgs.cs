﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HttpSniff.Sniffer {
	public class TcpStreamEventArgs : EventArgs {
		public enum TcpStreamEvent {
			Established, PacketArrival, PacketPush, Finished
		}

		public TcpStreamEvent EventType { get; private set; }
		public TcpStream Sender;
		public byte[] Buffer;
		public IPEndPoint Source;
		public IPEndPoint Destination;
		public bool Inbound = false;
		public bool Outbound = false;

		public TcpStreamEventArgs(TcpStream sender, TcpStreamEvent et, bool fromremtote) {
			this.EventType = et;
			this.Sender = sender;
			this.Inbound = fromremtote;
			this.Outbound = !fromremtote;
			if(fromremtote) {
				this.Source = sender.RemotehostEndpoint;
				this.Destination = sender.LocalhostEndpoint;
			}
			else {
				this.Source = sender.LocalhostEndpoint;
				this.Destination = sender.RemotehostEndpoint;
			}
		}

	}
}
