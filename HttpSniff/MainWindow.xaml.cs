﻿using HttpSniff.Logging;
using HttpSniff.Network;
using HttpSniff.Sniffer;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace HttpSniff {
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window {
		private TcpSniffer _Sniffer = new TcpSniffer();

		public MainWindow() {
			this._Sniffer.OnCaptured += new TcpCapturedHandler(OnCaptured);
			InitializeComponent();
			TextWriter tw = new RichTextBoxTextWriter(this.logbox);
			Logger.OutWriter = tw;
			Logger.ErrorWriter = tw;
			this._fwdlg = (new FirewallDialog(this._Sniffer._DropNetmask));
		}

		private delegate void AddTcpListDelegate(TcpPacket model);
		private void OnCaptured(TcpPacket packet) {
			this.tcplist.Dispatcher.Invoke(new AddTcpListDelegate(AddTcpList), packet);
		}

		private void AddTcpList(TcpPacket p) {
			this.tcplist.Items.Add(p);
			this.tcplist.ScrollIntoView(p);
		}

		private void StartClick(object sender, RoutedEventArgs e) {
			if(this._Sniffer.SnifferRunning) {
				this._Sniffer.Stop();
				((Button)sender).Content = "run";
			}
			else {
				this._Sniffer.Start();
				((Button)sender).Content = "stop";
			}
		}

		private void Window_Closed(object sender, EventArgs e) {
			this._Sniffer.Stop();
			this._fwdlg.Close();
		}

		private FirewallDialog _fwdlg;

		private void FirewallClick(object sender, RoutedEventArgs e) {
			this._fwdlg.Show();
		}
	}
}
