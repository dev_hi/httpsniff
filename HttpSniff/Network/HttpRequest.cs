﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HttpSniff.Extensions;

namespace HttpSniff.Network {
	public class HttpRequest {
		public string Method = "GET";
		public string URL = "/";
		public Dictionary<string, string> Headers = new Dictionary<string, string>();
		public byte[] PayloadData = new byte[0];

		public HttpRequest(IPEndPoint local, IPEndPoint remote, byte[] buf) {
			MemoryStream ms = new MemoryStream(buf);
			string fl = ms.ReadLine();
			string hv = fl.Substring(fl.Length - 8, 8);
			if(hv.Equals("HTTP/1.1")) {
				string[] tmp = fl.Split(' ');
				this.Method = tmp[0];
				this.URL = tmp[1];
				string line = ms.ReadLine();
				while(line.Length > 0) {
					int c = line.IndexOf(':');
					this.Headers.Add(line.Substring(0, c).Trim().ToLower(), line.Substring(c + 1).Trim());
					line = ms.ReadLine();
				}
				if(this.Headers.ContainsKey("content-type")) {
					int len = int.Parse(this.Headers["content-length"]);
					this.PayloadData = new byte[len];
					ms.Read(this.PayloadData, 0, len);
				}
			}
			else {
				throw new Exception("not http request");
			}
			ms.Close();
		}
	}
}
