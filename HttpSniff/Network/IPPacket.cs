﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HttpSniff.Network {
	public class IPPacket {
		[StructLayout(LayoutKind.Sequential)]
		internal struct _IPv4HeaderStruct {
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			internal byte[] dst;
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			internal byte[] src;
			internal ushort hdrchksum;
			internal byte protocol;
			internal byte ttl;
			internal ushort flags;
			internal ushort ident;
			internal ushort len;
			internal byte dsf;
			internal byte hdrlen;
		}

		public enum Version {
			IPv4 = 4,
			IPv6 = 6
		}
		public enum SubProtocol {
			ICMP = 0x01,
			IGMP = 0x02,
			TCP = 0x06,
			UDP = 0x11,
			Unknown
		}

		private _IPv4HeaderStruct _InternalStructure;

		public Version ProtocolVersion {
			get => (Version)((this._InternalStructure.hdrlen & 0xF0) >> 4);
			set => this._InternalStructure.hdrlen = (byte)(((byte)value << 4) | this.HeaderLength);
		}
		public int HeaderLength {
			get => this._InternalStructure.hdrlen & 0x0F;
			set => this._InternalStructure.hdrlen = (byte)(((byte)this.ProtocolVersion << 4) | value & 0x0F);
		}
		public byte DSFBit {
			get => this._InternalStructure.dsf;
			set => this._InternalStructure.dsf = value;
		}
		public ushort Length {
			get => this._InternalStructure.len;
			set => this._InternalStructure.len = value;
		}
		public ushort Identification {
			get => this._InternalStructure.ident;
			set => this._InternalStructure.ident = value;
		}
		public ushort FlagsBit {
			get => this._InternalStructure.flags;
			set => this._InternalStructure.flags = value;
		}
		public int TTL {
			get => this._InternalStructure.ttl;
			set => this._InternalStructure.ttl = (byte)value;
		}
		public SubProtocol Protocol {
			get => (SubProtocol)this._InternalStructure.protocol;
			set => this._InternalStructure.protocol = (byte)value;
		}
		public ushort HeaderChecksum {
			get => this._InternalStructure.hdrchksum;
			set => this._InternalStructure.hdrchksum = value;
		}
		public IPAddress SourceAddress {
			get => new IPAddress(this._InternalStructure.src);
			set => this._InternalStructure.src = value.GetAddressBytes();
		}
		public IPAddress DestinationAddress {
			get => new IPAddress(this._InternalStructure.dst);
			set => this._InternalStructure.dst = value.GetAddressBytes();
		}
		public byte[] PayloadData = new byte[0];
		public uint ReceivedLength = 0;

		public IPPacket() {
			this._InternalStructure = new _IPv4HeaderStruct();
		}

		public IPPacket(IntPtr buff, uint len, bool managed = false) {
			this.ReceivedLength = len;
			byte[] buf = new byte[20];
			Marshal.Copy(buff, buf, 0, 20);
			buf = buf.Reverse().ToArray();
			IntPtr bufff = Marshal.AllocHGlobal(buf.Length);
			Marshal.Copy(buf, 0, bufff, buf.Length);
			this._InternalStructure = (_IPv4HeaderStruct)Marshal.PtrToStructure(bufff, typeof(_IPv4HeaderStruct));
			Marshal.FreeHGlobal(bufff);
			this._InternalStructure.src = this._InternalStructure.src.Reverse().ToArray();
			this._InternalStructure.dst = this._InternalStructure.dst.Reverse().ToArray();
			uint data_len = len - 20;
			this.PayloadData = new byte[data_len];
			if(data_len > 0) Marshal.Copy((IntPtr)(buff.ToInt64() + 20), this.PayloadData, 0, (int)data_len);
			if(managed) Marshal.FreeHGlobal(buff);
		}

		public IPPacket(byte[] buf) {
			IntPtr buff = Marshal.AllocHGlobal(buf.Length);
			Marshal.Copy(buf, 0, buff, buf.Length);
			this._InternalStructure = (_IPv4HeaderStruct)Marshal.PtrToStructure(buff, typeof(_IPv4HeaderStruct));
			Marshal.FreeHGlobal(buff);
		}

		public byte[] GetBuffer() {
			int size = Marshal.SizeOf(this._InternalStructure);
			byte[] buf = new byte[size + this.PayloadData.Length];
			IntPtr pStruct = Marshal.AllocHGlobal(size);
			Marshal.StructureToPtr(this._InternalStructure, pStruct, false);
			Marshal.Copy(pStruct, buf, 0, size);
			Marshal.FreeHGlobal(pStruct);
			if(this.PayloadData.Length > 0) Array.Copy(this.PayloadData, 0, buf, size, this.PayloadData.Length);
			return buf;
		}
		private IntPtr CopyPointer() {
			IntPtr pStruct = Marshal.AllocHGlobal(Marshal.SizeOf(this._InternalStructure));
			Marshal.StructureToPtr(this._InternalStructure, pStruct, false);
			return pStruct;
		}

		public IPPacket Copy() {
			return new IPPacket(this.CopyPointer(), 0, true);
		}

		public override string ToString() {
			string version = this.ProtocolVersion == Version.IPv4 ? "IPv4" : "IPv6";
			string protocol = this.Protocol == SubProtocol.TCP ? "TCP" :
				this.Protocol == SubProtocol.UDP ? "UDP" :
				this.Protocol == SubProtocol.ICMP ? "ICMP" :
				this.Protocol == SubProtocol.IGMP ? "IGMP" : "Unknown";
			return String.Format("[{0}][{1}] {2} => {3} HDRLEN={4} LEN={5} BLEN={6}", version, protocol, this.SourceAddress, this.DestinationAddress, this.HeaderLength, this.Length, this.PayloadData.Length);
		}
	}
}
