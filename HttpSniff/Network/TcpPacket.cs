﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using HttpSniff.Extensions;

namespace HttpSniff.Network {
	public class TcpPacket {
		[StructLayout(LayoutKind.Sequential)]
		public struct _TcpHeaderStruct {
			public ushort urgptr;
			public ushort wndsize;
			public ushort chksum;
			public byte flags1;
			public byte flags2;
			public uint acknum;
			public uint seqnum;
			public ushort dstport;
			public ushort srcport;
		}

		public ushort SourcePort {
			get => this._InternalStruct.srcport;
			set => this._InternalStruct.srcport = value;
		}
		public ushort DestinationPort {
			get => this._InternalStruct.dstport;
			set => this._InternalStruct.dstport = value;
		}
		public uint SequenceNumber {
			get => this._InternalStruct.seqnum;
			set => this._InternalStruct.seqnum = value;
		}
		public uint AcknowledgmentNumber {
			get => this._InternalStruct.acknum;
			set => this._InternalStruct.acknum = value;
		}
		public ushort WindowSize {
			get => this._InternalStruct.wndsize;
			set => this._InternalStruct.wndsize = value;
		}
		public ushort Checksum {
			get => this._InternalStruct.chksum;
			set => this._InternalStruct.chksum = value;
		}
		public ushort UrgentPointer {
			get => this._InternalStruct.urgptr;
			set => this._InternalStruct.urgptr = value;
		}

		private _TcpHeaderStruct _InternalStruct;

		public byte[] Options = new byte[0];
		public byte[] PayloadData = new byte[0];
		public int PayloadDataLength => this.PayloadData.Length;

		public ushort Flags => this._InternalStruct.flags1;
		public byte DataOffset {
			get { return this._InternalStruct.flags2.GetBits(4, 4); }
			set { this._InternalStruct.flags2.SetBits(4, 4, value); }
		}
		public bool RSV3 {
			get { return this._InternalStruct.flags2.GetBit(3); }
			set { this._InternalStruct.flags2.SetBit(3, value); }
		}
		public bool RSV2 {
			get { return this._InternalStruct.flags2.GetBit(2); }
			set { this._InternalStruct.flags2.SetBit(2, value); }
		}
		public bool RSV1 {
			get { return this._InternalStruct.flags2.GetBit(1); }
			set { this._InternalStruct.flags2.SetBit(1, value); }
		}
		public bool NC {
			get { return this._InternalStruct.flags2.GetBit(0); }
			set { this._InternalStruct.flags2.SetBit(0, value); }
		}
		public bool CWR {
			get { return this._InternalStruct.flags1.GetBit(7); }
			set { this._InternalStruct.flags1.SetBit(7, value); }
		}
		public bool ECE {
			get { return this._InternalStruct.flags1.GetBit(6); }
			set { this._InternalStruct.flags1.SetBit(6, value); }
		}
		public bool URG {
			get { return this._InternalStruct.flags1.GetBit(5); }
			set { this._InternalStruct.flags1.SetBit(5, value); }
		}
		public bool ACK {
			get { return this._InternalStruct.flags1.GetBit(4); }
			set { this._InternalStruct.flags1.SetBit(4, value); }
		}
		public bool PSH {
			get { return this._InternalStruct.flags1.GetBit(3); }
			set { this._InternalStruct.flags1.SetBit(3, value); }
		}
		public bool RST {
			get { return this._InternalStruct.flags1.GetBit(2); }
			set { this._InternalStruct.flags1.SetBit(2, value); }
		}
		public bool SYN {
			get { return this._InternalStruct.flags1.GetBit(1); }
			set { this._InternalStruct.flags1.SetBit(1, value); }
		}
		public bool FIN {
			get { return this._InternalStruct.flags1.GetBit(0); }
			set { this._InternalStruct.flags1.SetBit(0, value); }
		}

		public IPPacket IP = null;
		public IPEndPoint SourceEndPoint => this.IP == null ? null : new IPEndPoint(this.IP.SourceAddress, this.SourcePort);
		public IPEndPoint DestinationEndPoint => this.IP == null ? null : new IPEndPoint(this.IP.DestinationAddress, this.DestinationPort);

		public TcpPacket() { this._InternalStruct = new _TcpHeaderStruct(); }
		public TcpPacket(IPPacket ip) : this(ip.PayloadData) {
			this.IP = ip;
		}

		public TcpPacket(byte[] data) {
			if(data.Length > 0) {
				MemoryStream ms = new MemoryStream(data);
				IntPtr buff = Marshal.AllocHGlobal(20);
				byte[] buf = new byte[20];
				ms.Read(buf, 0, 20);
				buf = buf.Reverse().ToArray();
				Marshal.Copy(buf, 0, buff, 20);
				this._InternalStruct = (_TcpHeaderStruct)Marshal.PtrToStructure(buff, typeof(_TcpHeaderStruct));
				Marshal.FreeHGlobal(buff);
				buf = null;
				int l = (this.DataOffset - 5) * 4;
				this.Options = new byte[l];
				if(this.Options.Length > 0)
					ms.Read(this.Options, 0, l);
				this.PayloadData = new byte[data.Length - 20 - l];
				if(this.PayloadData.Length > 0)
					ms.Read(this.PayloadData, 0, (int)(ms.Length - ms.Position));
				ms.Close();
			}
		}

		public void SetFlags1(byte flags) {
			this._InternalStruct.flags1 = flags;
		}
		public void SetFlags2(byte flags) {
			this._InternalStruct.flags2 = flags;
		}

		public byte[] GetBuffer() {
			int size = Marshal.SizeOf(this._InternalStruct);
			byte[] buf = new byte[size];
			IntPtr pStruct = Marshal.AllocHGlobal(size);
			Marshal.StructureToPtr(this._InternalStruct, pStruct, false);
			Marshal.Copy(pStruct, buf, 0, size);
			Marshal.FreeHGlobal(pStruct);
			MemoryStream ms = new MemoryStream();
			ms.Write(buf, 0, buf.Length);
			ms.Write(this.Options, 0, this.Options.Length);
			ms.Write(this.PayloadData, 0, this.PayloadData.Length);
			buf = ms.ToArray();
			ms.Close();
			return buf;
		}

		public string FlagString => GetFlagString();

		public string GetFlagString() {
			string NC = this.NC ? "[NC]" : "";
			string CWR = this.CWR ? "[CWR]" : "";
			string ECE = this.ECE ? "[ECE]" : "";
			string URG = this.URG ? "[SYN]" : "";
			string ACK = this.ACK ? "[ACK]" : "";
			string PSH = this.PSH ? "[PSH]" : "";
			string RST = this.RST ? "[RST]" : "";
			string SYN = this.SYN ? "[SYN]" : "";
			string FIN = this.FIN ? "[FIN]" : "";
			return String.Format("{0}{1}{2}{3}{4}{5}{6}", NC, CWR, URG, ACK, PSH, RST, SYN, FIN);
		}

		public override string ToString() {
			return String.Format("[TCP] {0} >> {1} SEQ={2} ACK={3} Flags=0x{4:X4}", this.SourcePort, this.DestinationPort, this.SequenceNumber, this.AcknowledgmentNumber, this.Flags);
		}
	}
}
