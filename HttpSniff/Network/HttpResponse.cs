﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HttpSniff.Extensions;

namespace HttpSniff.Network {
	public class HttpResponse {
		public IPEndPoint LocalEndPoint;
		public IPEndPoint RemoteEndPoint;
		public int ResponseCode = 0;
		public string ResponseMessage = "";
		public Dictionary<string, string> Headers = new Dictionary<string, string>();
		public byte[] PayloadData = new byte[0];

		public HttpResponse(IPEndPoint local, IPEndPoint remote, byte[] buf) {
			this.LocalEndPoint = local;
			this.RemoteEndPoint = remote;
			bool ishttp = false;
			bool isres = false;
			for(int i = 0; i < buf.Length - 4 && buf[i + 4] != 0x0A && buf[i + 4] != 0x0D; ++i) {
				ishttp |= buf[i] == 0x48 &&
				buf[i + 1] == 0x54 &&
				buf[i + 2] == 0x54 &&
				buf[i + 3] == 0x50 &&
				buf[i + 4] == 0x2f;
				if(i == 0 && ishttp) {
					isres = true;
				}
			}


			if(ishttp && isres) {
				MemoryStream ms = new MemoryStream(buf);
				string head = ms.ReadLine();
				string[] s = head.Split(' ');
				this.ResponseCode = int.Parse(s[1]);
				this.ResponseMessage = s[2];
				string line = ms.ReadLine();
				while(line.Length > 0) {
					int c = line.IndexOf(':');
					this.Headers.Add(line.Substring(0, c).Trim().ToLower(), line.Substring(c + 1).Trim());
					line = ms.ReadLine();
				}
				if(this.Headers.ContainsKey("content-type")) {
					int len = int.Parse(this.Headers["content-length"]);
					this.PayloadData = new byte[len];
					ms.Read(this.PayloadData, 0, len);
				}
				ms.Close();
			}
			else {
				throw new Exception("not http packet");
			}
		}
	}
}
