﻿using HttpSniff.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HttpSniff.Network {
	public class CIDRMask : ObservableCollection<string> {
		private uint[] _MaskBits = new uint[0];
		private uint[] _AddressBands = new uint[0];

		public CIDRMask() { }
		public CIDRMask(string mask) { this.AddMask(mask); }

		public bool CheckMask(IPAddress ip) {
			if(this.Count == 0) return true;
			if(ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
				byte[] ipb = ip.GetAddressBytes();
				uint addr = 0;
				for(int i = 0; i < ipb.Length; ++i) {
					addr |= ipb[i];
					if(i != 3) addr <<= 8;
				}
				for(int i = 0; i < this._MaskBits.Length; ++i) {
					if(((addr & this._MaskBits[i]) ^ this._AddressBands[i]) == 0) 
						return true;
				}
			}
			return false;
		}

		public void AddMask(string mask) {
			this.Add(mask);
			if(!mask.Contains('.')) return;
			string ipstr;
			int sbits;
			int slash = mask.IndexOf('/');
			if(slash == -1) {
				ipstr = mask;
				sbits = 32;
			}
			else {
				ipstr = mask.Substring(0, slash);
				sbits = int.Parse(mask.Substring(slash + 1));
			}
			string[] bytestr = ipstr.Split('.');
			uint bits = 0;
			for(int i = 0; i < bytestr.Length; ++i) {
				bits |= byte.Parse(bytestr[i]);
				if(i != 3) bits <<= 8;
			}
			uint ret = 0;
			for(int i = 0; i < 32; ++i) {
				if(i <= sbits) ret |= 1;
				ret <<= 1;
			}

			uint[] _new = new uint[this._MaskBits.Length + 1];
			Array.Copy(this._MaskBits, 0, _new, 0, this._MaskBits.Length);
			_new[this._MaskBits.Length] = ret; //(bits & ret)
			this._MaskBits = _new;

			_new = new uint[this._AddressBands.Length + 1];
			Array.Copy(this._AddressBands, 0, _new, 0, this._AddressBands.Length);
			_new[this._AddressBands.Length] = (bits & ret);
			this._AddressBands = _new;
		}

		public void RemoveMask(int i) {
			this.RemoveAt(i);
			uint[] a = new uint[this._AddressBands.Length - 1];
			uint[] m = new uint[this._MaskBits.Length - 1];
			for(int j = 0; j < this._AddressBands.Length; ++j) {
				if(j < i) {
					a[j] = this._AddressBands[j];
					m[j] = this._MaskBits[j];
				}
				else if(j > i) {
					a[j - 1] = this._AddressBands[j];
					m[j - 1] = this._MaskBits[j];
				}
			}
			this._AddressBands = a;
			this._MaskBits = m;
		}

		public int Count => this._AddressBands.Length;

		public string GetMaskString(int i) {
			IPAddress addr = new IPAddress((long)this._AddressBands[i]);
			int count = this._MaskBits[i].BitCount();
			return String.Format("{0}/{1}", addr, count);
		}
	}
}
