﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace WinDivert {

	/*
	 WinDivert.h porting
	 */
		
	using OVERLAPPED = NativeOverlapped;
	using LPOVERLAPPED = NativeOverlapped;
	using ULONG_PTR = UIntPtr;
	using HANDLE = IntPtr;
	using UINT8 = Byte;
	using BOOL = Boolean;
	using UINT = UInt32;
	using INT32 = Int32;
	using UINT16 = UInt16;
	using UINT32 = UInt32;
	using UINT64 = UInt64;
	using INT64 = Int64;
	using DWORD = UInt64;
	using INT16 = Int16;

	internal enum WINDIVERT_LAYER {
		NETWORK = 0,        /* Network layer. */
		NETWORK_FORWARD = 1,/* Network layer (forwarded packets) */
		FLOW = 2,           /* Flow layer. */
		SOCKET = 3,         /* Socket layer. */
		REFLECT = 4,        /* Reflect layer. */
	}


	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_DATA_NETWORK {
		internal UINT32 IfIdx;                       /* Packet's interface index. */
		internal UINT32 SubIfIdx;                    /* Packet's sub-interface index. */
	}

	/*
	 * WinDivert FLOW layer data.
	 */
	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_DATA_FLOW {
		internal UINT64 EndpointId;                  /* Endpoint ID. */
		internal UINT64 ParentEndpointId;            /* Parent endpoint ID. */
		internal UINT32 ProcessId;                   /* Process ID. */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32[] LocalAddr;                /* Local address. */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32[] RemoteAddr;               /* Remote address. */
		internal UINT16 LocalPort;                   /* Local port. */
		internal UINT16 RemotePort;                  /* Remote port. */
		internal UINT8  Protocol;                    /* Protocol. */
	}

	/*
	 * WinDivert SOCKET layer data.
	 */
	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_DATA_SOCKET {
		internal UINT64 EndpointId;                  /* Endpoint ID. */
		internal UINT64 ParentEndpointId;            /* Parent Endpoint ID. */
		internal UINT32 ProcessId;                   /* Process ID. */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32[] LocalAddr;                /* Local address. */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32[] RemoteAddr;               /* Remote address. */
		internal UINT16 LocalPort;                   /* Local port. */
		internal UINT16 RemotePort;                  /* Remote port. */
		internal UINT8  Protocol;                    /* Protocol. */
	}

	/*
	 * WinDivert REFLECTION layer data.
	 */
	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_DATA_REFLECT {
		internal INT64  Timestamp;                   /* Handle open time. */
		internal UINT32 ProcessId;                   /* Handle process ID. */
		internal WINDIVERT_LAYER Layer;              /* Handle layer. */
		internal UINT64 Flags;                       /* Handle flags. */
		internal INT16  Priority;                    /* Handle priority. */
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct WINDIVERT_ADDRESS {
		public INT64 Timestamp;                  /* Packet's timestamp. */
		public UINT32 Layer;                     /* Packet's layer. */
		public UINT32 Event;                     /* Packet event. */
		public UINT32 Sniffed;                   /* Packet was sniffed? */
		public UINT32 Outbound;                  /* Packet is outound? */
		public UINT32 Loopback;                  /* Packet is loopback? */
		public UINT32 Impostor;                  /* Packet is impostor? */
		public UINT32 IPv6;                      /* Packet is IPv6? */
		public UINT32 IPChecksum;                /* Packet has valid IPv4 checksum? */
		public UINT32 TCPChecksum;               /* Packet has valid TCP checksum? */
		public UINT32 UDPChecksum;               /* Packet has valid UDP checksum? */
		public UINT32 Reserved1;
		public UINT32 Reserved2;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public UINT8[] Reserved3;
	}

	[StructLayout(LayoutKind.Explicit)]
	internal struct WINDIVERT_ADDRESS_Reserved3 { //union
		[FieldOffset(0)]
		internal WINDIVERT_DATA_NETWORK Network;  /* Network layer data. */
		[FieldOffset(0)]
		internal WINDIVERT_DATA_FLOW Flow;        /* Flow layer data. */
		[FieldOffset(0)]
		internal WINDIVERT_DATA_SOCKET Socket;    /* Socket layer data. */
		[FieldOffset(0)]
		internal WINDIVERT_DATA_REFLECT Reflect;  /* Reflect layer data. */
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		[FieldOffset(0)]
		internal UINT8[] Data;
	}

	internal enum WINDIVERT_EVENT {
		NETWORK_PACKET = 0, /* Network packet. */
		FLOW_ESTABLISHED = 1,
		/* Flow established. */
		FLOW_DELETED = 2,   /* Flow deleted. */
		SOCKET_BIND = 3,    /* Socket bind. */
		SOCKET_CONNECT = 4, /* Socket connect. */
		SOCKET_LISTEN = 5,  /* Socket listen. */
		SOCKET_ACCEPT = 6,  /* Socket accept. */
		SOCKET_CLOSE = 7,   /* Socket close. */
		REFLECT_OPEN = 8,   /* WinDivert handle opened. */
		REFLECT_CLOSE = 9,  /* WinDivert handle closed. */
	}
	internal enum WINDIVERT_PARAM {
		QUEUE_LENGTH = 0,   /* Packet queue length. */
		QUEUE_TIME = 1,     /* Packet queue time. */
		QUEUE_SIZE = 2,     /* Packet queue size. */
		VERSION_MAJOR = 3,  /* Driver version (major). */
		VERSION_MINOR = 4,  /* Driver version (minor). */
	}

	internal enum WINDIVERT_SHUTDOWN {
		RECV = 0x1,      /* Shutdown recv. */
		SEND = 0x2,      /* Shutdown send. */
		BOTH = 0x3,      /* Shutdown recv and send. */
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_IPHDR {
		internal UINT8  HdrLength;
		internal UINT8  Version;
		internal UINT8  TOS;
		internal UINT16 Length;
		internal UINT16 Id;
		internal UINT16 FragOff0;
		internal UINT8  TTL;
		internal UINT8  Protocol;
		internal UINT16 Checksum;
		internal UINT32 SrcAddr;
		internal UINT32 DstAddr;
	}

	[StructLayout(LayoutKind.Sequential)]
	struct WINDIVERT_IPV6HDR {
		internal UINT8  TrafficClass0;
		internal UINT8  Version;
		internal UINT8  FlowLabel0;
		internal UINT8  TrafficClass1;
		internal UINT16 FlowLabel1;
		internal UINT16 Length;
		internal UINT8  NextHdr;
		internal UINT8  HopLimit;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32 SrcAddr;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		internal UINT32 DstAddr;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_ICMPHDR {
		internal UINT8  Type;
		internal UINT8  Code;
		internal UINT16 Checksum;
		internal UINT32 Body;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_ICMPV6HDR {
		internal UINT8  Type;
		internal UINT8  Code;
		internal UINT16 Checksum;
		internal UINT32 Body;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_TCPHDR {
		internal UINT16 SrcPort;
		internal UINT16 DstPort;
		internal UINT32 SeqNum;
		internal UINT32 AckNum;
		internal UINT16 Reserved1;
		internal UINT16 HdrLength;
		internal UINT16 Fin;
		internal UINT16 Syn;
		internal UINT16 Rst;
		internal UINT16 Psh;
		internal UINT16 Ack;
		internal UINT16 Urg;
		internal UINT16 Reserved2;
		internal UINT16 Window;
		internal UINT16 Checksum;
		internal UINT16 UrgPtr;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDIVERT_UDPHDR {
		internal UINT16 SrcPort;
		internal UINT16 DstPort;
		internal UINT16 Length;
		internal UINT16 Checksum;
	}

	public static class WinDivertExports {

		/****************************************************************************/
		/* Windows.h PORTING CONSTANTS                                              */
		/****************************************************************************/

		private const object NULL = null;
		private const int INVALID_HANDLE_VALUE = -1;
		private const int ERROR_INVALID_PARAMETER = 0x57;
		private const int EXIT_SUCCESS = 0;
		private const int EXIT_FAILURE = 1;
		private const bool TRUE = true;
		private const bool FALSE = false;

		/****************************************************************************/
		/* WINDIVERT API                                                            */
		/****************************************************************************/


		internal const int WINDIVERT_FLAG_SNIFF      =      0x0001;
		internal const int WINDIVERT_FLAG_DROP       =      0x0002;
		internal const int WINDIVERT_FLAG_RECV_ONLY  =      0x0004;
		internal const int WINDIVERT_FLAG_READ_ONLY  =      WINDIVERT_FLAG_RECV_ONLY;
		internal const int WINDIVERT_FLAG_SEND_ONLY  =      0x0008;
		internal const int WINDIVERT_FLAG_WRITE_ONLY =      WINDIVERT_FLAG_SEND_ONLY;
		internal const int WINDIVERT_FLAG_NO_INSTALL =      0x0010;
		internal const int WINDIVERT_FLAG_FRAGMENTS  =      0x0020;


		internal const WINDIVERT_PARAM WINDIVERT_PARAM_MAX = WINDIVERT_PARAM.VERSION_MINOR;
		internal const WINDIVERT_SHUTDOWN WINDIVERT_SHUTDOWN_MAX = WINDIVERT_SHUTDOWN.BOTH;

		/*
		 * Open a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertOpen", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.SysInt)]
		internal static extern HANDLE WinDivertOpen(
			[In]					string filter,
			[In]					WINDIVERT_LAYER layer,
			[In]					INT16 priority,
			[In]					UINT64 flags = 1);


		/*
		 * Receive (read) a packet from a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint ="WinDivertRecv", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertRecv(
			[In]					HANDLE handle,
			[Out, Optional]			byte[] pPacket,
			[In]					UINT packetLen,
			[In, Out, Optional]		ref UINT pRecvLen,
			[In, Out, Optional]		ref WINDIVERT_ADDRESS pAddr);

		/*
		 * Receive (read) a packet from a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertRecvEx", SetLastError = true, CharSet = CharSet.Ansi)]
		[return:MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertRecvEx(
			[In]					HANDLE handle,
			[Out, Optional]			byte[] pPacket,
			[In]					UINT packetLen,
			[In, Out, Optional]		ref UINT pRecvLen,
			[In]					UINT64 flags,
			[In, Out]				ref WINDIVERT_ADDRESS pAddr,
			[In, Out, Optional]		ref UINT pAddrLen,
			[In, Out, Optional]		IntPtr lpOverlapped);
			/*
		internal static BOOL WinDivertRecvExo(HANDLE handle, byte[] packet, UINT packetLen, ref UINT recvLen, UINT64 flags, ref WINDIVERT_ADDRESS addr, ref UINT addrLen, LPOVERLAPPED overlapped) {
			IntPtr pPacket;
			Marshal.StructureToPtr(packet, pPacket, );
			return WinDivertRecvEx(handle, pPacket, packetLen, pRecvLen, flags, pAddr, pAddrLen);
		}*/

		/*
		 * Send (write/inject) a packet to a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertSend", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertSend(
			[In]					HANDLE handle,
			[In]					byte[] pPacket,
			[In]					UINT packetLen,
			[In, Out, Optional]		ref UINT pSendLen,
			[In]					ref WINDIVERT_ADDRESS pAddr);

		/*
		 * Send (write/inject) a packet to a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertSendEx", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertSendEx(
			[In]					HANDLE handle,
			[In]					byte[] pPacket,
			[In]					UINT packetLen,
			[In, Out, Optional]		ref UINT pSendLen,
			[In]					UINT64 flags,
			[In]					ref WINDIVERT_ADDRESS pAddr,
			[In]					ref UINT addrLen,
			[In, Out, Optional]		ref LPOVERLAPPED lpOverlapped);

		/*
		 * Shutdown a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertShutdown", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertShutdown(
			[In]					HANDLE handle,
			[In]					WINDIVERT_SHUTDOWN how);

		/*
		 * Close a WinDivert handle.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertClose", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertClose(
			[In]					HANDLE handle);

		/*
		 * Set a WinDivert handle parameter.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertSetParam", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertSetParam(
			[In]					HANDLE handle,
			[In]					WINDIVERT_PARAM param,
			[In]					UINT64 value);

		/*
		 * Get a WinDivert handle parameter.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertGetParam", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern BOOL WinDivertGetParam(
			[In]					HANDLE handle,
			[In]					WINDIVERT_PARAM param,
			[In, Out]				ref UINT64 pValue);
																											   
		internal const int WINDIVERT_PRIORITY_HIGHEST            =  30000									   ;
		internal const int WINDIVERT_PRIORITY_LOWEST             =  (-WINDIVERT_PRIORITY_HIGHEST)			   ;
		internal const int WINDIVERT_PARAM_QUEUE_LENGTH_DEFAULT  =  4096									   ;
		internal const int WINDIVERT_PARAM_QUEUE_LENGTH_MIN      =  32										   ;
		internal const int WINDIVERT_PARAM_QUEUE_LENGTH_MAX      =  16384									   ;
		internal const int WINDIVERT_PARAM_QUEUE_TIME_DEFAULT    =  2000        /* 2s */					   ;
		internal const int WINDIVERT_PARAM_QUEUE_TIME_MIN        =  100         /* 100ms */					   ;
		internal const int WINDIVERT_PARAM_QUEUE_TIME_MAX        =  16000       /* 16s */					   ;
		internal const int WINDIVERT_PARAM_QUEUE_SIZE_DEFAULT    =  4194304     /* 4MB */					   ;
		internal const int WINDIVERT_PARAM_QUEUE_SIZE_MIN        =  65535       /* 64KB */					   ;
		internal const int WINDIVERT_PARAM_QUEUE_SIZE_MAX        =  33554432    /* 32MB */					   ;
		internal const int WINDIVERT_BATCH_MAX                   =  0xFF        /* 255 */					   ;
		internal const int WINDIVERT_MTU_MAX                     =  (40 + 0xFFFF)                              ;

		/****************************************************************************/
		/* WINDIVERT HELPER API                                                     */
		/****************************************************************************/


		internal static UINT16 WINDIVERT_IPHDR_GET_FRAGOFF(WINDIVERT_IPHDR hdr) { return (UINT16)(((hdr).FragOff0) & 0xFF1F); }
		internal static BOOL WINDIVERT_IPHDR_GET_MF(WINDIVERT_IPHDR hdr) { return ((((hdr).FragOff0) & 0x0020) != 0); }
		internal static BOOL WINDIVERT_IPHDR_GET_DF(WINDIVERT_IPHDR hdr) { return ((((hdr).FragOff0) & 0x0040) != 0); }
		internal static BOOL WINDIVERT_IPHDR_GET_RESERVED(WINDIVERT_IPHDR hdr) { return ((((hdr).FragOff0) & 0x0080) != 0); }
		internal static void WINDIVERT_IPHDR_SET_FRAGOFF(WINDIVERT_IPHDR hdr, UINT16 val) {
			do {
				(hdr).FragOff0 = (UINT16)(((((hdr).FragOff0) & 0x00E0)) | 
					((val) & 0xFF1F));
			} while(FALSE);
		}
		internal static void WINDIVERT_IPHDR_SET_MF(WINDIVERT_IPHDR hdr, UINT16 val) {
			do {
				(hdr).FragOff0 = (UINT16)(((((hdr).FragOff0) & 0xFFDF)) |
					(((val) & 0x0001) << 5));
			} while(FALSE);
		}
		internal static void WINDIVERT_IPHDR_SET_DF(WINDIVERT_IPHDR hdr, UINT16 val) {
			do {
				(hdr).FragOff0 = (UINT16)(((((hdr).FragOff0) & 0xFFDF)) |
					(((val) & 0x0001) << 6));
			} while(FALSE);
		}
		internal static void WINDIVERT_IPHDR_SET_RESERVED(WINDIVERT_IPHDR hdr, UINT16 val) {
			do {
				(hdr).FragOff0 = (UINT16)(((((hdr).FragOff0) & 0xFFDF)) |
					(((val) & 0x0001) << 7));
			} while(FALSE);
		}

		internal static UINT8 WINDIVERT_IPV6HDR_GET_TRAFFICCLASS(WINDIVERT_IPV6HDR hdr) { return (UINT8)((((hdr).TrafficClass0) << 4) | ((hdr).TrafficClass1)); }
		internal static UINT32 WINDIVERT_IPV6HDR_GET_FLOWLABEL(WINDIVERT_IPV6HDR hdr) { return (UINT32)((((UINT32)(hdr).FlowLabel0) << 16) | ((UINT32)(hdr).FlowLabel1)); }

		/*
		 * Flags for WinDivertHelperCalcChecksums()
		 */
		internal const int WINDIVERT_HELPER_NO_IP_CHECKSUM                   = 1  ;
		internal const int WINDIVERT_HELPER_NO_ICMP_CHECKSUM                 = 2  ;
		internal const int WINDIVERT_HELPER_NO_ICMPV6_CHECKSUM               = 4  ;
		internal const int WINDIVERT_HELPER_NO_TCP_CHECKSUM                  = 8  ;
		internal const int WINDIVERT_HELPER_NO_UDP_CHECKSUM                  = 16 ;

		/*
		 * Hash a packet.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHashPacket", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT64 WinDivertHelperHashPacket(
			[In]        byte[] pPacket,
			[In]        UINT packetLen,
			[In]        UINT64 seed = 0);

		/*
		 * Parse IPv4/IPv6/ICMP/ICMPv6/TCP/UDP headers from a raw packet.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHashPacket", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperParsePacket(
			[In]				byte[] pPacket,
			[In]				UINT packetLen,
			[In, Out, Optional]		ref WINDIVERT_IPHDR ppIpHdr,
			[In, Out, Optional]		ref WINDIVERT_IPV6HDR ppIpv6Hdr,
			[In, Out, Optional]		ref UINT8 pProtocol,
			[In, Out, Optional]		ref WINDIVERT_ICMPHDR ppIcmpHdr,
			[In, Out, Optional]		ref WINDIVERT_ICMPV6HDR ppIcmpv6Hdr,
			[In, Out, Optional]		ref WINDIVERT_TCPHDR ppTcpHdr,
			[In, Out, Optional]		ref WINDIVERT_UDPHDR ppUdpHdr,
			[In, Out, Optional]		ref byte[] ppData,
			[In, Out, Optional]		ref UINT pDataLen,
			[In, Out, Optional]		ref byte[] ppNext,
			[In, Out, Optional]		ref UINT pNextLen);

		/*
		 * Parse an IPv4 address.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperParseIPv4Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperParseIPv4Address(
			[In]						string addrStr,
			[In, Out, Optional]			ref UINT32[] pAddr);

		/*
		 * Parse an IPv6 address.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperParseIPv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperParseIPv6Address(
			[In]						string addrStr,
			[In, Out, Optional]			ref UINT32[] pAddr);

		/*
		 * Format an IPv4 address.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperFormatIPv4Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperFormatIPv4Address(
			[In]					UINT32 addr,
			[In, Out]				ref byte[] buffer,
			[In]					ref UINT bufLen);

		/*
		 * Format an IPv6 address.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperFormatIPv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperFormatIPv6Address(
			[In]			ref UINT32[] pAddr,
			[In, Out]       ref byte[] buffer,
			[In]			UINT bufLen);

		/*
		 * Calculate IPv4/IPv6/ICMP/ICMPv6/TCP/UDP checksums.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperCalcChecksums", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperCalcChecksums(
			[In, Out]				ref byte[] pPacket,
			[In]					UINT packetLen,
			[In, Out, Optional]		ref WINDIVERT_ADDRESS pAddr,
			[In]					UINT64 flags);

		/*
		 * Decrement the TTL/HopLimit.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperDecrementTTL", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperDecrementTTL(
			[In, Out]				ref byte[] pPacket,
			[In]					UINT packetLen);

		/*
		 * Compile the given filter string.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperCompileFilter", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperCompileFilter(
			[In]					string filter,
			[In]					WINDIVERT_LAYER layer,
			[Out, Optional]			byte[] _object,
			[In]					UINT objLen,
			[In, Out, Optional]		ref string errorStr,
			[In, Out, Optional]		ref IntPtr errorPos);

		/*
		 * Evaluate the given filter string.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperFormatFilter", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperEvalFilter(
			[In]        string filter,
			[In]        byte[] pPacket,
			[In]        UINT packetLen,
			[In]        ref WINDIVERT_ADDRESS pAddr);

		/*
		 * Format the given filter string.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperFormatFilter", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern BOOL WinDivertHelperFormatFilter(
			[In]        string filter,
			[In]        WINDIVERT_LAYER layer,
			[Out]       byte[] buffer,
			[In]        UINT bufLen);

		/*
		 * Byte ordering.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperNtohs", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT16 WinDivertHelperNtohs(
			[In] UINT16 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHtons", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT16 WinDivertHelperHtons(
			[In] UINT16 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperNtohl", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT32 WinDivertHelperNtohl(
			[In] UINT32 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHtonl", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT32 WinDivertHelperHtonl(
			[In] UINT32 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperNtohll", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT64 WinDivertHelperNtohll(
			[In] UINT64 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHtonll", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern UINT64 WinDivertHelperHtonll(
			[In] UINT64 x);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperNtohIPv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void WinDivertHelperNtohIPv6Address(
			[Out]      UINT[] inAddr,
			[In]       ref UINT[] outAddr);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHtonIPv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void WinDivertHelperHtonIPv6Address(
			[Out]      UINT[] inAddr,
			[In]       ref UINT[] outAddr);

		/*
		 * Old names to be removed in the next version.
		 */
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperNtohIpv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void WinDivertHelperNtohIpv6Address(
			[Out]        UINT[] inAddr,
			[In]       ref UINT[] outAddr);
		[DllImport("WinDivert.dll", EntryPoint = "WinDivertHelperHtonIpv6Address", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void WinDivertHelperHtonIpv6Address(
			[Out]        UINT[] inAddr,
			[In]       ref UINT[] outAddr);
	}
}
