﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WinDivert;

namespace WinDivert.Invoke {
	public static class WinDivertInvoke {
		[DllImport("WinDivertInvoke.dll", EntryPoint = "StartSniff", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern IntPtr StartSniff(string filter, int mtu, short priority);
		[DllImport("WinDivertInvoke.dll", EntryPoint = "StopSniff", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void StopSniff(IntPtr hDivert);
		[DllImport("WinDivertInvoke.dll", EntryPoint = "IsRunning", SetLastError = true, CharSet = CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool IsRunning();
		[DllImport("WinDivertInvoke.dll", EntryPoint = "SetCallback", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void _SetCallback(SniffCallback cb);
		[DllImport("WinDivertInvoke.dll", EntryPoint = "SetErrorCallback", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern void _SetErrorCallback(ErrorCallback cb);
		[DllImport("WinDivertInvoke.dll", EntryPoint = "InjectPacket", SetLastError = true, CharSet = CharSet.Ansi)]
		internal static extern uint InjectPacket(IntPtr hDivert, byte[] packet, uint packet_len, WINDIVERT_ADDRESS[] addr, uint addr_len);

		private static SniffCallback _SniffCallback;
		private static ErrorCallback _ErrorCallback;
		internal static void SetCallback(SniffCallback cb) {
			_SniffCallback = cb;
			_SetCallback(_SniffCallback);
		}
		internal static void SetErrorCallback(ErrorCallback cb) {
			_ErrorCallback = cb;
			_SetErrorCallback(_ErrorCallback);
		}

		public delegate bool SniffCallback(IntPtr pPacket, uint packet_len, WINDIVERT_ADDRESS[] pAddr, uint addr_len);
		public delegate void ErrorCallback(ulong lasterror);
	}
}
