﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpSniff.Logging {
	public static class Logger {
		public static FileStream LogFile = new FileStream("./output.log", FileMode.Append);
		public static TextWriter ErrorWriter = Console.Error;
		public static TextWriter OutWriter = Console.Out;

		public static void Log() {
			Log("");
		}
		public static void Log(string message, params object[] args) {
			Log(ConsoleColor.White, message, args);
		}
		public static void Warn(string message, params object[] args) {
			Log(ConsoleColor.Yellow, message, args);
		}
		public static void Error(string message, params object[] args) {
			Log(ErrorWriter, ConsoleColor.Red, message, args);
		}
		public static void Log(ConsoleColor c, string message, params object[] args) {
			Log(OutWriter, c, message, args);
		}
		public static void Log(TextWriter iostream, ConsoleColor c, string message, params object[] args) { 
			string output = DateTime.Now.ToString("[yyyy\\/MM\\/dd HH:mm:ss] ") + String.Format(message, args);
			Console.ForegroundColor = c;
			iostream.WriteLine(output);
			byte[] buf = Encoding.ASCII.GetBytes(output + "\r\n");
			if(LogFile != null) {
				LogFile.Write(buf, 0, buf.Length);
				LogFile.Flush();
			}
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}
