﻿// dllmain.cpp : DLL 애플리케이션의 진입점을 정의합니다.

#ifndef UNICODE
#define UNICODE
#endif // !UNICODE

#include "pch.h"

#define INVOKE_EXPORT __declspec(dllexport)
#define CALLBACK_PARAMS const VOID* pPacket, UINT packet_len, WINDIVERT_ADDRESS* pAddr, UINT addr_len
#define CALLBACK_CPARAMS pPacket, packet_len, pAddr, addr_len

using std::thread;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved);
INVOKE_EXPORT HANDLE StartSniff(const char* filter, int mtu, short priority);
INVOKE_EXPORT UINT InjectPacket(HANDLE hDivert, UINT8* packet, UINT packet_len, WINDIVERT_ADDRESS* addr, UINT addr_len);
INVOKE_EXPORT void StopSniff(HANDLE hDivert);
INVOKE_EXPORT bool IsRunning();
INVOKE_EXPORT void SetCallback(bool (*cb)(CALLBACK_PARAMS));
INVOKE_EXPORT void SetErrorCallback(void (*cb)(DWORD p));
DWORD WINAPI SniffThreadWork(LPVOID arg);
bool FireSniffCallback(CALLBACK_PARAMS);
void FireErrorCallback(DWORD lasterror);

static bool (*SniffCallback)(CALLBACK_PARAMS) = NULL;
static void (*ErrorCallback)(DWORD lasterror) = NULL;
static HANDLE RunningThread;
static bool Running = false;

typedef struct {
	HANDLE hDivert;
	int mtu;
	int batch;
} CONFIG, * PCONFIG;

static CONFIG config;

INVOKE_EXPORT HANDLE StartSniff(const char* filter, int mtu, short priority) {
	if(!Running) {
		//CONFIG config;
		config.batch = 1;
		config.mtu = mtu;
		config.hDivert = WinDivertOpen(filter, WINDIVERT_LAYER_NETWORK, (INT16)priority, 0);
		if(config.hDivert == INVALID_HANDLE_VALUE) {
			FireErrorCallback(GetLastError());
		}
		else {
			RunningThread = CreateThread(NULL, 1, (LPTHREAD_START_ROUTINE)SniffThreadWork, &config, 0, NULL);
			//SniffThreadWork(&config);
			Running = true;
		}

		return config.hDivert;
	}
	return INVALID_HANDLE_VALUE;
}

INVOKE_EXPORT UINT InjectPacket(HANDLE hDivert, UINT8* packet, UINT packet_len, WINDIVERT_ADDRESS* addr, UINT addr_len) {
	if(Running && hDivert != INVALID_HANDLE_VALUE) {
		UINT send_len = 0;

		if(!WinDivertSendEx(hDivert, packet, packet_len, &send_len, 0, addr, addr_len, NULL))
			FireErrorCallback(GetLastError());
		else
			return send_len;
	}
	return 0;
}

INVOKE_EXPORT void StopSniff(HANDLE hDivert) {
	if(hDivert != INVALID_HANDLE_VALUE) {
		WinDivertClose(hDivert);
		if(RunningThread != INVALID_HANDLE_VALUE) {
			CloseHandle(RunningThread);
		}
	}
	Running = false;
}

INVOKE_EXPORT bool IsRunning() {
	return Running;
}

INVOKE_EXPORT void SetCallback(bool (*cb)(CALLBACK_PARAMS)) {
	SniffCallback = cb;
}

bool FireSniffCallback(CALLBACK_PARAMS) {
	if(SniffCallback != NULL) {
		return SniffCallback(CALLBACK_CPARAMS);
	}
	else return true;
}

INVOKE_EXPORT void SetErrorCallback(void (*cb)(DWORD p)) {
	ErrorCallback = cb;
}

void FireErrorCallback(DWORD lasterror) {
	if(ErrorCallback != NULL) ErrorCallback(lasterror);
}


DWORD WINAPI SniffThreadWork(LPVOID arg) {
	UINT8* packet;
	UINT packet_len, addr_len;
	WINDIVERT_ADDRESS* addr;
	//CONFIG config = *(PCONFIG)arg;
	HANDLE handle;
	int batch, mtu;

	handle = config.hDivert;
	batch = config.batch;
	mtu = config.mtu;

	packet = (UINT8*)malloc(batch * mtu);
	addr = (WINDIVERT_ADDRESS*)malloc(batch * sizeof(WINDIVERT_ADDRESS));

	if(packet == NULL || addr == NULL)
		FireErrorCallback(GetLastError());
	else while(Running) {
		packet_len = batch * mtu;
		addr_len = batch * sizeof(WINDIVERT_ADDRESS);
		if(!WinDivertRecvEx(handle, packet, packet_len, &packet_len, 0, addr, &addr_len, NULL))
			FireErrorCallback(GetLastError());
		else {
			if(FireSniffCallback(packet, packet_len, addr, addr_len)) {
				if(!WinDivertSendEx(handle, packet, packet_len, &packet_len, 0, addr, addr_len, NULL))
					FireErrorCallback(GetLastError());
			}
		}
	}

	return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	switch(ul_reason_for_call) {
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

